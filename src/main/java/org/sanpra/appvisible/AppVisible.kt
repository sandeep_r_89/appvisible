package org.sanpra.appvisible

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.startup.Initializer
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject
import java.util.concurrent.TimeUnit

private abstract class AbstractActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {
    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityResumed(activity: Activity) {
    }

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }
}

private class MyActivityLifecycleCallbacks : AbstractActivityLifecycleCallbacks() {
    private val activityVisibleEvents = PublishSubject.create<Int>()
    val activityVisibleObservable : Observable<Boolean> = activityVisibleEvents.debounce(1, TimeUnit.SECONDS).map { it!=0 }.distinctUntilChanged()

    var visibleActivityCount : Int = 0
        private set(value) {
            field = value
            activityVisibleEvents.onNext(value)
        }

    override fun onActivityStarted(activity: Activity) {
        visibleActivityCount++
    }

    override fun onActivityStopped(activity: Activity) {
        visibleActivityCount--
    }
}

private val activityLifecycleCallbacks = MyActivityLifecycleCallbacks()

val activityVisibleObservable : Observable<Boolean> = activityLifecycleCallbacks.activityVisibleObservable

@Suppress("unused")
internal class AppVisibleInitializer : Initializer<Observable<Boolean>> {
    override fun create(context: Context): Observable<Boolean> {
        (context.applicationContext as Application).registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
        return activityLifecycleCallbacks.activityVisibleObservable
    }

    override fun dependencies(): MutableList<Class<out Initializer<*>>> = mutableListOf()
}